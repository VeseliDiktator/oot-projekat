using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Concept_SplashScreen
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {

        DispatcherTimer tajmer = new DispatcherTimer();

        public SplashScreen()
        {
            InitializeComponent();

            tajmer.Tick += new EventHandler(tajmer_tick);
            tajmer.Interval = new TimeSpan(0, 0, 2);
            tajmer.Start();
        }

        private void tajmer_tick(object sender, EventArgs e)
        {
            StartPage startPage = new StartPage();
            startPage.Show();

            tajmer.Stop();
            this.Close();
        }
    }
}
